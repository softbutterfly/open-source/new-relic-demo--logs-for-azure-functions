const fs = require('fs');
// const forwarder = require('./forwarder.js');
const forwarder2 = require('./forwarder2.js');

const text = fs.readFileSync('./sample-blob.json', 'utf8');

context = {
    "log": {
        "info": console.log,
        "warn": console.log,
        "error": console.log,
    },
    executionContext: {
        "functionName": "localExecutionFunction",
        "invocationId": "localExecutionFunction",
    }
}

console.log(forwarder2(context, text))
